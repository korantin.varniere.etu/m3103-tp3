package tp3;

public class Cellule {

	private int x, y;
	private Cellule parent;
	
	public Cellule(int x, int y, Cellule parent) {
		this.x = x;
		this.y = y;
		this.parent = parent;
	}
	
	public Cellule(int x, int y) {
		this(x, y, null);
	}
	
	public Cellule(int[] pos) {
		this(pos[0], pos[1]);
	}
	
	public void setParent(Cellule c) {
		this.parent = c;
	}
	
	public Cellule getParent() {
		return this.parent;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	public int[] getPos() {
		return new int[] {this.x, this.y};
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cellule other = (Cellule) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "(" + this.x + ", " + this.y + ")";
	}
	
}
