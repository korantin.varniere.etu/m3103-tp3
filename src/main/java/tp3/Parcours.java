package tp3;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Random;
import java.util.Stack;

public class Parcours {
	
//	private static Labyrinthe l;
//	private static Stack<Cellule> p;
//	private static boolean exitFound = false;
//
//	public static void main(String[] args) {
//		initLabAndStack();
//		addAndMarkCell(new Cellule(l.entree()));
//		
//		while (!exitFound && !p.isEmpty()) {
//			Cellule c = p.peek();
//			if (l.estSortie(c.getPos())) {
//				exitFound = true;
//			} else {
//				Cellule c2 = neighborOf(c);
//				
//				try {Thread.sleep(1) ;}catch(InterruptedException e){}
//				
//				if (c2 == null) {
//					p.pop();
//				} else {
//					addAndMarkCell(c2);
//				}
//			}
//		}
//		
//		if (exitFound) {
//			markRoad();
//			System.out.println("Sortie trouvée !");
//		} else {
//			System.out.println("Il n'y a pas de chemin reliant l'entrée à la sortie !");
//		}
//	}
//	
//	private static void initLabAndStack() {
//		l = new Labyrinthe();
//		p = new Stack<Cellule>();
//	}
//	
//	private static void addAndMarkCell(Cellule c) {
//		p.push(c);
//		l.poserMarque(c.getPos());
//	}
//	
//	private static Cellule neighborOf(Cellule c) {
//		Cellule c2 = null;
//		List<Cellule> cells = new ArrayList<Cellule>();
//		List<Cellule> trueCells = new ArrayList<Cellule>();
//		
//		int dx = -1;
//		int dy = -1;
//		
//		if (c.getX()+dx > 0) {
//			cells.add(new Cellule(c.getX()+dx, c.getY()));
//		}
//		if (c.getY()+dy > 0) {
//			cells.add(new Cellule(c.getX(), c.getY()+dy));
//		}
//		
//		dx = 1;
//		dy = 1;
//		
//		if (c.getX()+dx < l.n()) {
//			cells.add(new Cellule(c.getX()+dx, c.getY()));
//		}
//		if (c.getY()+dy < l.n()) {
//			cells.add(new Cellule(c.getX(), c.getY()+dy));
//		}
//		
//		for (int i = 0; i < cells.size(); i++) {
//			if (!(l.estMarque(cells.get(i).getPos()) || l.estMur(cells.get(i).getPos()))) {
//				trueCells.add(cells.get(i));
//			}
//		}
//		
//		Random rand = new Random();
//		try {
//			c2 = trueCells.get(rand.nextInt(trueCells.size()));
//		} catch (Exception e) {
//			return null;
//		}
//		
//		
//		System.out.println(c2);
//		
//		return c2;
//	}
//	
//	private static void markRoad() {
//		for (Cellule cellule : p) {
//			l.poserMarqueChemin(cellule.getPos());
//		}
//	}
//	
	private static Labyrinthe l;
	private static Queue<Cellule> p;
	private static boolean exitFound = false;

	public static void main(String[] args) {
		initLabAndStack();
		addAndMarkCell(new Cellule(l.entree()));
		
		while (!exitFound && !p.isEmpty()) {
			Cellule c = p.peek();
			if (l.estSortie(c.getPos())) {
				exitFound = true;
			} else {
				Cellule c2 = neighborOf(c);
				
				try {Thread.sleep(1) ;}catch(InterruptedException e){}
				
				if (c2 == null) {
					p.poll();
				} else {
					c2.setParent(c);
					addAndMarkCell(c2);
				}
			}
		}
		
		if (exitFound) {
			markRoad();
			System.out.println("Sortie trouvée !");
		} else {
			System.out.println("Il n'y a pas de chemin reliant l'entrée à la sortie !");
		}
	}
	
	private static void initLabAndStack() {
		l = new Labyrinthe();
		p = new ArrayDeque<Cellule>();
	}
	
	private static void addAndMarkCell(Cellule c) {
		p.offer(c);
		l.poserMarque(c.getPos());
	}
	
	private static Cellule neighborOf(Cellule c) {
		Cellule c2 = null;
		List<Cellule> cells = new ArrayList<Cellule>();
		List<Cellule> trueCells = new ArrayList<Cellule>();
		
		int dx = -1;
		int dy = -1;
		
		if (c.getX()+dx > 0) {
			cells.add(new Cellule(c.getX()+dx, c.getY()));
		}
		if (c.getY()+dy > 0) {
			cells.add(new Cellule(c.getX(), c.getY()+dy));
		}
		
		dx = 1;
		dy = 1;
		
		if (c.getX()+dx < l.n()) {
			cells.add(new Cellule(c.getX()+dx, c.getY()));
		}
		if (c.getY()+dy < l.n()) {
			cells.add(new Cellule(c.getX(), c.getY()+dy));
		}
		
		for (int i = 0; i < cells.size(); i++) {
			if (!(l.estMarque(cells.get(i).getPos()) || l.estMur(cells.get(i).getPos()))) {
				trueCells.add(cells.get(i));
			}
		}
		
		Random rand = new Random();
		try {
			c2 = trueCells.get(rand.nextInt(trueCells.size()));
		} catch (Exception e) {
			return null;
		}
		
		return c2;
	}
	
	private static void markRoad() {
		Cellule c = p.peek();
		while (c.getParent() != null) {
			l.poserMarqueChemin(c.getPos());
			c = c.getParent();
		}
		l.poserMarqueChemin(l.entree());
	}
	
}
